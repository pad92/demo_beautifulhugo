# Beautiful Hugo with Staticman comment reply
## Build your personal blog with comment replies for free on GitLab

View demo at https://vincenttam.gitlab.io/bhdemo ([source][3])

## Simple setup

1. Clone this repo

       git clone https://gitlab.com/VincentTam/bhdemo.git <your-site-name>

2. Remove existing comments under the folder `data` and the file `LICENSE`.
3. Modify the following fields in the Hugo config file `config.toml`.  You may comment out stuff by `#`.

       baseURL = "https://<username>.gitlab.io"
       title = "Your title"

       [Params]
         subtitle = "Your subtitle"

       [Params.staticman]
         api = "https://staticman3.herokuapp.com/v3/entry/gitlab/<username>/<username>.gitlab.io/master/comments"
         pulls = "https://gitlab.com/<username>/<username>.gitlab.io/merge_requests"

       [Author]
         name = "Your name"
         website = "https://example.com"
    If this is your project page, use the following parameters instead.

       baseURL = "https://<username>.gitlab.io/<your-project>"

       [Params.staticman]
         api = "https://staticman3.herokuapp.com/v3/entry/gitlab/<username>/<your-project>/master/comments"
         pulls = "https://gitlab.com/<username>/<your-project>/merge_requests"


4. Either remove the [reCAPTCHA][6] config or *change the parameters below with your own site key and secret*.  (You may apply your personal ones with your Google account.)

       [Params.staticman.recaptcha]
         sitekey = "6Lcv8G8UAAAAAEqV1Y-XEPum00C_DxhD6O--qkFo"
         secret = "p5uHlH9hCqp...33F1WaIYuwNw=="
5. Remove these lines at the bottom of `config.toml`.

       [[menu.main]]
         name = "Source"
         url = "https://gitlab.com/vincenttam/bhdemo"
         weight = 2

Write new posts.

1. Execute the command `hugo new posts/<your-filename>.md`
2. Edit `content/posts/<your-filename>.md`
3. Save everything with `git add .`
4. Commit with `git commit`.  Add your commit message.
5. Publish with `git push -u origin master`

  [3]: https://gitlab.com/vincenttam/bhdemo
  [6]: https://www.google.com/recaptcha/intro/v3.html

## Credits

1. [Beautiful Hugo][bh] by Michael Romero
2. [Staticman][sm] by Eduardo Bouças
3. [Staticman PR 219][sm219] by Nicolas Tsim
4. [Network Hobo][nb] by Dan C Williams

[bh]: https://github.com/halogenica/beautifulhugo
[sm]: https://github.com/eduardoboucas/staticman
[sm219]: https://github.com/eduardoboucas/staticman/pull/219
[nb]: https://github.com/dancwilliams/networkhobo